<?php

use Illuminate\Database\Seeder;

class Vendas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vendas')->insert([
            'numero_venda'  => 1,
            'data_venda' => '2010-06-11',
            'tipo_venda' => 'Prazo',
            'codigo_cliente' => 2,
            'data_vencimento' => '2010-07-11'
        ]);

        DB::table('vendas')->insert([
            'numero_venda'  => 2,
            'data_venda' => '2010-06-15',
            'tipo_venda' => 'Vista',
            'codigo_cliente' => -1,
            'data_vencimento' => '2010-06-15'
        ]);

        DB::table('vendas')->insert([
            'numero_venda'  => 3,
            'data_venda' => '2010-08-20',
            'tipo_venda' => 'Vista',
            'codigo_cliente' => -1,
            'data_vencimento' => '2010-08-20'
        ]);

        DB::table('vendas')->insert([
            'numero_venda'  => 4,
            'data_venda' => '2010-08-25',
            'tipo_venda' => 'Prazo',
            'codigo_cliente' => 5,
            'data_vencimento' => '2010-10-25'
        ]);
    }
}
