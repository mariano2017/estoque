<?php

use Illuminate\Database\Seeder;

class Clientes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('clientes')->insert([
            'id_cliente' => 2,
            'nome_cliente'  => 'Manoel',
            'endereco' => 'Rua da padaria',
            'telefone' => '1111-2222'
        ]);

        DB::table('clientes')->insert([
            'id_cliente' => 1,
            'nome_cliente'  => 'Pedro',
            'endereco' => 'Rua da farmácia',
            'telefone' => '3333-4444'
        ]);

        DB::table('clientes')->insert([
            'id_cliente' => 5,
            'nome_cliente'  => 'Luzia',
            'endereco' => 'Rua da hospital',
            'telefone' => '1234-5678'
        ]);

        DB::table('clientes')->insert([
            'id_cliente' => 10,
            'nome_cliente'  => 'José',
            'endereco' => 'Rua da supermercado',
            'telefone' => '4321-0000'
        ]);
    }
}
