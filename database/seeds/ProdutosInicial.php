<?php

use Illuminate\Database\Seeder;

class ProdutosInicial extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('produtos')->insert([
            'id_produto' => 1,
            'descricao_produto'  => 'Lápis',
            'valor_compra' => 1.00,
            'valor_venda' => 2.00,
            'qtd_estoque' => 100,
            'estoque_minimo' => 20
        ]);

        DB::table('produtos')->insert([
            'id_produto' => 2,
            'descricao_produto'  => 'Caneta',
            'valor_compra' => 2.00,
            'valor_venda' => 3.00,
            'qtd_estoque' => 80,
            'estoque_minimo' => 15
        ]);

        DB::table('produtos')->insert([
            'id_produto' => 3,
            'descricao_produto'  => 'Apontador',
            'valor_compra' => 0.50,
            'valor_venda' => 1.30,
            'qtd_estoque' => 200,
            'estoque_minimo' => 30
        ]);
    }
}
