<?php

use Illuminate\Database\Seeder;

class ItemVendas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('item_vendas')->insert([
            'numero_venda' => 1,
            'id_produto'  => 2,
            'qtd_vendida' => 5,
            'preco_pago' => 2.70
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 1,
            'id_produto'  => 3,
            'qtd_vendida' => 4,
            'preco_pago' => 1.30
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 2,
            'id_produto'  => 1,
            'qtd_vendida' => 20,
            'preco_pago' => 1.50
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 3,
            'id_produto'  => 1,
            'qtd_vendida' => 5,
            'preco_pago' => 1.50
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 3,
            'id_produto'  => 2,
            'qtd_vendida' => 10,
            'preco_pago' => 2.00
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 3,
            'id_produto'  => 3,
            'qtd_vendida' => 9,
            'preco_pago' => 1.10
        ]);

        DB::table('item_vendas')->insert([
            'numero_venda' => 4,
            'id_produto'  => 2,
            'qtd_vendida' => 5,
            'preco_pago' => 3.00
        ]);
    }
}
