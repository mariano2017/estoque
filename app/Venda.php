<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    protected $fillable = ['numero_venda', 'data_venda', 'tipo_venda', 'codigo_cliente', 'data_vencimento'];
}
