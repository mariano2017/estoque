<?php


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('RepositoryInterface', 'app\Repositories\RepositoryInterface');
    }

    public function provides()
    {
        return array();
    }

}
