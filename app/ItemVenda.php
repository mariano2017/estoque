<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemVenda extends Model
{
    protected $fillable = ['numero_venda', 'codigo_produto', 'qtd_vendida', 'preco_pago'];
}
