<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemVendaRequest;
use App\Repositories\Repository;
use App\ItemVenda;

class ItemVendaController extends Controller
{
    protected $model;

    public function __construct(ItemVenda $model)
    {
        $this->model = new Repository($model);
    }

    public function index()
    {
        return $this->model->all();
    }

    public function create(ItemVendaRequest $request)
    {

        return ($this->model->create($request->all())) ?
            redirect()->back()->with(['status'=>'Item de Venda cadastrado com sucesso!']) :
            redirect()->back()->with(['status'=>'Erro ao cadastrar']);

    }

    public function read($id)
    {
        return $this->model->find($id);
    }

    public function update(ItemVendaRequest $request, $id)
    {
        return ($this->model->update($request->all(), $id)) ?
            redirect()->back()->with('status', 'Item de Venda atualizado com sucesso!') :
            redirect()->back()->with('status', 'Erro ao atualizar!');
    }

    public function delete($id)
    {
        return ($this->model->delete($id)) ?
            redirect()->back()->with('status', 'Item de Venda deletado com sucesso!') :
            redirect()->back()->with('status', 'Erro ao deletar!');
    }
}
