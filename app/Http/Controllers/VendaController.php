<?php

namespace App\Http\Controllers;

use App\Venda;
use App\Http\Requests\VendaRequest;
use App\Repositories\Repository;

class VendaController extends Controller
{
    protected $model;

    public function __construct(Venda $model)
    {
        $this->model = new Repository($model);
    }

    public function index()
    {
        return $this->model->all();
    }

    public function create(VendaRequest $request)
    {

        return ($this->model->create($request->all())) ?
            redirect()->back()->with(['status'=>'Venda cadastrada com sucesso!']) :
            redirect()->back()->with(['status'=>'Erro ao cadastrar']);

    }

    public function read($id)
    {
        return $this->model->find($id);
    }

    public function update(VendaRequest $request, $id)
    {
        return ($this->model->update($request->all(), $id)) ?
            redirect()->back()->with('status', 'Venda atualizada com sucesso!') :
            redirect()->back()->with('status', 'Erro ao atualizar!');
    }

    public function delete($id)
    {
        return ($this->model->delete($id)) ?
            redirect()->back()->with('status', 'Venda deletada com sucesso!') :
            redirect()->back()->with('status', 'Erro ao deletar!');
    }
}
