<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClienteRequest;
use App\Repositories\Repository;
use App\Cliente;

class ClienteController extends Controller
{
    protected $model;

    public function __construct(Cliente $model)
    {
        $this->model = new Repository($model);
    }

    public function index()
    {
        return $this->model->all();
    }

    public function create(ClienteRequest $request)
    {

        return ($this->model->create($request->all())) ?
            redirect()->back()->with(['status'=>'Cliente cadastrado com sucesso!']) :
            redirect()->back()->with(['status'=>'Erro ao cadastrar']);

    }

    public function read($id)
    {
        return $this->model->find($id);
    }

    public function update(ClienteRequest $request, $id)
    {
        return ($this->model->update($request->all(), $id)) ?
            redirect()->back()->with('status', 'Cliente atualizado com sucesso!') :
            redirect()->back()->with('status', 'Erro ao atualizar!');
    }

    public function delete($id)
    {
        return ($this->model->delete($id)) ?
            redirect()->back()->with('status', 'Cliente deletado com sucesso!') :
            redirect()->back()->with('status', 'Erro ao deletar!');
    }
}
