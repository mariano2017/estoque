<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProdutoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'descrição_produto'=> 'required|min:5',
        'valor_compra'=> 'required|min:1',
        'valor_venda'=> 'required|min:1',
        'qtd'=> 'required|min:1',
        'qtd_minima'=> 'required|min:1',
    ];
    }
}
