<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $fillable = ['descricao_produto', 'valor_compra', 'valor_venda', 'qtd_estoque', 'estoque_minimo'];
}
