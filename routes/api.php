<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'produto'], function(){
    Route::get('/', 'ProdutoController@index');
    Route::post('/', 'ProdutoController@create');
    Route::get('/{id}', 'ProdutoController@read');
    Route::put('/{id}', 'ProdutoController@update');
    Route::delete('/{id}', 'ProdutoController@delete');
});

Route::group(['prefix' => 'cliente'], function(){
    Route::get('/', 'ClienteController@index');
    Route::post('/', 'ClienteController@create');
    Route::get('/{id}', 'ClienteController@read');
    Route::put('/{id}', 'ClienteController@update');
    Route::delete('/{id}', 'ClienteController@delete');
});

Route::group(['prefix' => 'venda'], function(){
    Route::get('/', 'VendaController@index');
    Route::post('/', 'VendaController@create');
    Route::get('/{id}', 'VendaController@read');
    Route::put('/{id}', 'VendaController@update');
    Route::delete('/{id}', 'VendaController@delete');
});

Route::group(['prefix' => 'item_venda'], function(){
    Route::get('/', 'ItemVendaController@index');
    Route::post('/', 'ItemVendaController@create');
    Route::get('/{id}', 'ItemVendaController@read');
    Route::put('/{id}', 'ItemVendaController@update');
    Route::delete('/{id}', 'ItemVendaController@delete');
});
